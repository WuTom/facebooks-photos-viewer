package com.example.photosviewer

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity

import com.example.photosviewer.R


import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }
}

